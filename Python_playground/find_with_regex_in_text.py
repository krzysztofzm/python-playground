#! python3

"""
created by: krzysztofzm based on exercise from Automating the boring stuff in Python
Description: script takes a text from clip board, modifies it and puts it back to the clip board.
            In this case it's searching for an given regex in copied text
"""

import pyperclip
import re

text = pyperclip.paste()
print(text)
search_for = input("type regexp expresson: ")
find_text = re.compile(r'%s' %search_for)

lines = text.split('\n')
search_result = find_text.findall(text)

print(search_result)
text = '\n'.join(search_result)
print(text)
pyperclip.copy(text)
