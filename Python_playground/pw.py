#! python3

"""
created by: krzysztofzm based on exercise from Automating the boring stuff in Python
Description: simple, usecure password manager.
            Password are stored in 'Passwords' dictionary.
            to copy password type in CLI (or press win+R in windows) pw and key from dictionary.
            E.g.: tocopy the mail password type 'pw email'
"""

import sys
import pyperclip

Passwords = {
    'email': 'efgnjve56r88-0ivcxz3IO',
    'blog': 'JHGr5orMj*^uh;ftf78'
}

if len(sys.argv) < 2:
    print('Użycie programu pw.py [konto] - skopiowanie hasła wskazanegi konta')
    sys.exit()

account = sys.argv[1]

if account in Passwords:
    pyperclip.copy(Passwords[account])
    print('Hasło do konta '+ account + ' zostało skopiowane do schowka.')
else:
    print('Nie istnieje konto o nazwie ' + account +'.')