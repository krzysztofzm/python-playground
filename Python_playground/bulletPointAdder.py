#! python3

"""
created by: krzysztofzm based on exercise from Automating the boring stuff in Python
Description: script takes a text from clip board, modifies it and puts it back to the clip board.
            In this case it's adding bullet points at the beggining  of each line
"""

#test comment

import pyperclip

text = pyperclip.paste()
print(text)

lines = text.split('\n')
for i in range(len(lines)):
    lines[i] = '* ' + lines[i]
    print(lines[i])

print(lines)
text = '\n'.join(lines)
print(text)
pyperclip.copy(text)