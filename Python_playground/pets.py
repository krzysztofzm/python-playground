""" Pets is a tamagotchi type text game. Names of the pets are in the pet_dic dictionary and the commands are available in the comm_dic. Type the name of the pet first and then the commend see what happens.

This game was created to practice object oriented programming. This are my first steps in object oriented programming. 

Created by Krzysztof Żminkowski (c) 2017
"""
import random 

class pet:
    def __init__(self, animal, name, sound, eats):
        self.animal = animal 
        self.name = name
        self.sound = sound
        self.eats = eats 
        self._hungry = False 
        self._play = False
        
    def does(self):
        if len(self.sound)>0:
            print (self.sound)
        else:
            print (self.name + " doesn't make any sound.")
    
    def who(self):
        return self.name
    
    @property
    def setHungry(self):
        return  self._hungry
    
    @property
    def setPlay(self):
        return  self._play
        
    @setHungry.setter
    def setHungry(self, value):
        if value == True or value == False:
            self._hungry = value
        else:
            raise TypeError("Invalide Type, must be Boolean")
            
    @setPlay.setter
    def setPlay(self, value):
        if value == True or value == False:
            self._play = value
        else:
            raise TypeError("Invalide Type, must be Boolean")
            
    def says(self):
        if self._hungry == True:
            i = random.randint(0, len(self.eats))
            print("I'm hungry! Please feed me! I want " +  self.eats[i] +"!!!")
        if self._play == True:
            print("I want to Play. Come and play with me.")
        if (self._hungry | self._play) == False:
            print("I'm fine, thanks. I'm going to sleep.")
    
    def likes_to_eat(self):
        i= random.randint(0, len(self.eats))
        print("I'd like to eat " + str(self.eats[i]) + ".")

def saing(petname):
    if petname in pet_dic:
        pname = pet_dic[petname]
        return pname.says()
        
def goes(petname):
    if petname in pet_dic:
        pname = pet_dic[petname]
        return pname.does()
    
def likes(petname):
    if petname in pet_dic:
        pname = pet_dic[petname]
        return pname.likes_to_eat()
        
def hunger(petname):
    if petname in pet_dic:
        pname = pet_dic[petname]
        pname.setHungry = True
        return saing(petname)
        
def playing(petname):
    if petname in pet_dic:
        pname = pet_dic[petname]
        pname.setPlay  = True
        return saing(petname)

Funkiel = pet("dog", "Funkiel", "Hau-hau!", ["meat", "rice", "bread", "carots", "left overs"])

Kadis = pet("cat", "Kadis", "Miaaauuuu!", ["fish", "meat", "cat food", "rice"])

comm_dic = {
   "say" : saing,
   "goes": goes,
   "hungry": hunger,
   "play": playing,
   "likes": likes
}

pet_dic = {
    "Funkiel" : Funkiel,
    "Kadis" : Kadis
}

def getinput():
    command = input().split()
    name = command[0]
    maincom = command[1]
    #react =""
    #if pet in pet_dic:
    #    pet = pet_dic[pet]
    #else:
    #    print("I don't know '{}'. Who is he?".format(pet))
    if maincom in comm_dic:
        react = comm_dic[maincom]
        return react(name)
    else:
        print("I don't uderstend '{}'".format(maincom))

#while True:
getinput()