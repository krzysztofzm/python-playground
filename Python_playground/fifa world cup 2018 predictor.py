import random

input_json = {
    "rankings":{
        "Argentina":5,
        "Australia":36,
        "Belgium":3,
        "Brazil":2,
        "Colombia":16,
        "Costa_Rica":23,
        "Croatia":20,
        "Denmark":12,
        "Egypt":45,
        "England":12,
        "France":7,
        "Germany":1,
        "Iceland":22,
        "Iran":37,
        "Japan":61,
        "Korea_Republic":57,
        "Mexico":15,
        "Morocco":41,
        "Nigeria":48,
        "Panama":55,
        "Peru":11,
        "Poland":8,
        "Portugal":4,
        "Russia":70,
        "Saudi_Arabia":67,
        "Senegal":27,
        "Serbia":34,
        "Spain":10,
        "Sweden":24,
        "Switzerland":6,
        "Tunisia":21,
        "Uruguay":14
    },
    "groups":{
        "A":[
            "Egypt",
            "Russia",
            "Saudi_Arabia",
            "Uruguay"
        ],
        "B":[
            "Iran",
            "Morocco",
            "Portugal",
            "Spain"
        ],
        "C":[
            "Australia",
            "Denmark",
            "France",
            "Peru"
        ],
        "D":[
            "Argentina",
            "Croatia",
            "Iceland",
            "Nigeria"
        ],
        "E":[
            "Brazil",
            "Costa_Rica",
            "Serbia",
            "Switzerland"
        ],
        "F":[
            "Germany",
            "Korea_Republic",
            "Mexico",
            "Sweden"
        ],
        "G":[
            "Belgium",
            "England",
            "Panama",
            "Tunisia"
        ],
        "H":[
            "Colombia",
            "Japan",
            "Poland",
            "Senegal"
        ]
    }
}

ranking = input_json["rankings"]
group = input_json["groups"]
group_points = {}
group_result = {}
champion = {}
draw = 2.5 #draw factor
n = 1000

def team_rank(team, fifa_nation=211, me = 0, sd = 7.5):
    return ((fifa_nation - ranking[team]) / fifa_nation * 100) + random.gauss(me, sd)

def game_winner(team1, team2):
    team1_rank = team_rank(team1)
    team2_rank = team_rank(team2)
    if team1_rank > team2_rank:
        return team1
    else:
        return team2

#print("group fase result\n")
for i in range(n):
    for key in group:
    #populating group_points dict with keys and value 0 (zero)
        group_points[key] = dict.fromkeys(group[key], 0)
    #each team from group is compered with other teams from the group
        for i in range(len(group[key])-1):
        #iteration through all other teams left in the group
            team1 = group[key][i]
            for team2 in group[key][i+1:]:
                team1_rank = team_rank(team1)
                team2_rank = team_rank(team2)
                if team1_rank > team2_rank + draw:
                    group_points[key][team1]+=3
                elif team1_rank + draw < team2_rank:
                    group_points[key][team2]+=3
                else:
                    group_points[key][team1]+=1
                    group_points[key][team2]+=1
    #populating the dict with sorted group fase result for each group
        group_result[key] = sorted(group_points[key], key=group_points[key].__getitem__, reverse = True)
#KO fase
    #print("\nKO faze  \n")

    winner8_1 = game_winner(group_result["A"][0], group_result["B"][1])
#print("1/8 final 1:",group_result["A"][0],"-", group_result["B"][1], "winner:", winner8_1)
    winner8_2 = game_winner(group_result["B"][0], group_result["A"][1])
#print("1/8 final 2:",group_result["B"][0],"-", group_result["A"][1], "winner:", winner8_2)
    winner8_3 = game_winner(group_result["C"][0], group_result["D"][1])
#print("1/8 final 3:",group_result["C"][0],"-", group_result["D"][1], "winner:", winner8_3)
    winner8_4 = game_winner(group_result["D"][0], group_result["C"][1])
#print("1/8 final 4:",group_result["D"][0],"-", group_result["C"][1], "winner:", winner8_4)
    winner8_5 = game_winner(group_result["E"][0], group_result["F"][1])
#print("1/8 final 5:",group_result["E"][0],"-", group_result["F"][1], "winner:", winner8_5)
    winner8_6 = game_winner(group_result["F"][0], group_result["E"][1])
#print("1/8 final 6:",group_result["F"][0],"-", group_result["E"][1], "winner:", winner8_6)
    winner8_7 = game_winner(group_result["G"][0], group_result["H"][1])
#print("1/8 final 7:",group_result["G"][0],"-", group_result["H"][1], "winner:", winner8_7)
    winner8_8 = game_winner(group_result["H"][0], group_result["G"][1])
#print("1/8 final 8:",group_result["H"][0],"-", group_result["G"][1], "winner:",winner8_8,"\n")

    winner4_1 = game_winner(winner8_1, winner8_3)
#print("1/4 final 1:", winner8_1, "-", winner8_3, "winner:", winner4_1)
    winner4_2 = game_winner(winner8_2, winner8_4)
#print("1/4 final 2:", winner8_2, "-", winner8_4, "winner:", winner4_2)
    winner4_3 = game_winner(winner8_5, winner8_7)
#print("1/4 final 3:", winner8_5, "-", winner8_7, "winner:", winner4_3)
    winner4_4 = game_winner(winner8_6, winner8_8)
#print("1/4 final 4:", winner8_6, "-", winner8_8, "winner:", winner4_4,"\n")
    semifinal1 = game_winner(winner4_1, winner4_3)
#print("semifinal 1:", winner4_1, "-", winner4_3, "winner:", semifinal1)
    semifinal2 = game_winner(winner4_2, winner4_4)
#print("semifinal 2:", winner4_2, "-", winner4_4, "winner:", semifinal2, "\n")
    final = game_winner(semifinal1, semifinal2)
#print("final:", semifinal1, "-", semifinal2, "\n\nThe world champion is:", final)
    if final in champion:
        champion[final] += 1
    else:
        champion[final] = 1
champion_sort= sorted(champion, key=champion.__getitem__, reverse = True)
for team in champion_sort:
    print(team, champion[team])