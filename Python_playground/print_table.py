#! python3
"""
created by: krzysztofzm
Description: prints list of lists as a table
"""

tableData = [['jabłka', 'pomarańcze', 'wiśnie', 'banany'],
             ['Alicja', 'Bob', 'Karol', 'Dawid'],
             ['psy', 'koty', 'łosie', 'gęsi']]


def print_table(table):
    col_widths = [0] * len(table)
    output_table = []
    for i in range(len(table)):
        for j in range(len(table[i])):
            if col_widths[i] < len(table[i][j]):
                col_widths[i] = len(table[i][j])
    for i in range(len(table[0])):
        temp_table = []
        for j in range(len(table)):
            temp_table.append(table[j][i].rjust(col_widths[j]))
        output_table.append(temp_table)
    for list in output_table:
        print(' '.join(list))

    return output_table

print_table(tableData)

