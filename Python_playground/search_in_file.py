"""
created by: krzysztofzm
Description: searches for a phrase 'search_for' in files with a given extension 'file_extension'
in a given directory 'walk_dir'. It returns a 'output.txt' with a list of files that were searched
and a 'file_path.txt' with list of files where the searched string was found.
"""

import os

# search_for = 'Z001TT6Y'
search_for = 'xml-stylesheet'
file_extension = '.html'

try:
    os.remove('output.txt')
    os.remove('file_paths.txt')
except:
    pass

walk_dir = 'C:\\Polarion\\polarion' #\\Sandbox\\PD_PA_Sandbox_SITRANS_TDL\\PD_PA_Sandbox_SITRANS_TDL\\modules\\20 Define Product Requirements and Architecture\\System Requirement Specification'

for root, subdirs, files in os.walk(walk_dir):
    with open('output.txt', 'a') as o:
        o.write('\n--\nroot = ' + root)
        o.write('\nsubdirs: ' + str(subdirs))
        o.write('\nfiles: ' + str(files))

    if len(files) > 0:
        for file in files:
            if file.endswith(file_extension):
                with open(os.path.join(root, file), encoding="utf8", errors='ignore') as f:
                    if search_for in f.read():
                        with open('file_paths.txt', 'a') as a:
                            a.write('\n' + os.path.join(root, file))